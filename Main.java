package org.example;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int number = random.nextInt(1, 101);
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter your name: ");
        String name = sc.nextLine();
        System.out.println("Let the game begin");
        int[] array = new int[100];
        int i = 0;
        while (true) {
            try{
                int input = sc.nextInt();
                array[i] = input;
                if (input < number) {
                    System.out.println("Your number is too small, please try again");
                } else if (input > number) {
                    System.out.println("Your number is too big, please try again");
                } else {
                    System.out.println("Congrulations, " + name);
                    int[] arr = sort(array);
                    System.out.println("Your numbers: ");
                    for(int k =0;k<arr.length;k++){
                        if(array[k]==0){
                            break;
                        }
                        System.out.print(arr[k]+" ");
                    }
                    break;
                }
                i++;
            }catch(InputMismatchException e){
                System.out.println("Invalid input , please try again");
                sc.next();
            }
        }
    }

    public static int[] sort(int[] array) {
        int count=0;
        for(int i = 0;i<array.length;i++){
            if(array[i]==0){
                break;
            }
            count++;
        }
        int[] arr = new int[count];
        for(int k = 0;k<array.length;k++){
            if(array[k]==0){
                break;
            }
            arr[k]=array[k];
        }
        int temp = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] > arr[i]) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        return arr;
    }
}